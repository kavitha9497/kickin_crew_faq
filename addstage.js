var unirest = require('unirest'),
	strformat = require('strformat'),
	moment = require('moment-business-days'),
	cheerio = require('cheerio'),
	path = require("path"),
	fs = require('fs'),
	libxmljs = require('libxmljs'),
	xmlJS = require('xml-js')

/**
 * function to update elastic DB
 **/
function updateElastic(req, res) {
	var updateEl = require("./updateelastic.js");
	updateEl['processapi'](req, res)
		.then(function (response) {
		})
		.catch(function (err) {
			//not returning any response, as the error handling is taken care in updateElastic API
			// chnaged promise function to normal unirest as the function 'updateElastic' is not handled in the below as promise, so when the promisse was rejected earlier, the server failed - JAI - 24 Jan 2019
		});
}
module.exports = {
	/******* Add New Stage to Workflow *******/
	addstage: function (req, res, wfXML) {
		var o = req.body;
		// check if we have the bare minimum to handle the requested
		// this means we need the customer id, project id and the current stage
		if (typeof (o.customer) === 'undefined' || o.customer === '' || typeof (o.project) === 'undefined' || o.project === '' || typeof (o.doi) === 'undefined' || o.doi === '') {
			res.send({
				status: {
					code: 500,
					message: "One or more of required parameters (customer id, project id, current stage) is/are not provided. requested action on stage  cannot be done. Unexpected input"
				}
			}).end();
			return false;
		}
		this.processapi(req, res, wfXML)
			.then(function (response) {
				res.send(response).end();
			})
			.catch(function (err) {
				res.status(400).send(err).end();
			});
	},
	processapi: function (req, res, wfmXML) {
		return new Promise(function (resolve, reject) {
			req.body.updatingXML = req.body.doi;
			if(wfmXML){
				let $ = cheerio.load(wfmXML);
				if(wfmXML || !req.body.stagename){
					req.body.stageName = $('stage').first().text();
					req.body.stagename = $('stage').first().text();
					req.body.updateWordCount = $('stage').first().attr('update-word-count');
				}
				if(wfmXML || !req.body.customerStage){
					req.body.customerStage = $('customer-stage').first().text();
				}

				//get the user to assign new stage which is configured in workflow trigger
				let assignNode = $('assign').first();
				var assignName = false;
				var assignEmail = false;
				if(assignNode.length > 0){
					assignName = assignNode.find('username').text();
					assignEmail = assignNode.find('useremail').text();					
				}
			}
			//get workflow
			getArticle(req, res)
				.then(function (response) {
					if (!response || typeof(response)!=='string'){
						expLogger('addstage: getXMLdata failed '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
						reject({
								status: {
									code: '400',
									message: 'Could not fetch data from database'
								},
								step: "add stage"
							});
						return false;
					} else {
						var wfXML = response;
						if (typeof (wfXML) != 'string' || wfXML == '') {
							expLogger('addstage: getstageXMLdata failed '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
							reject({
								status: {
									code: '400',
									message: 'Addstage failed: stageXML :' + wfXML
								},
								step: "add stage"
							});
							return false;
						}
						//load article into  dom
						var doc = libxmljs.parseXml(response)
						// get project and then stage template from it
						getProjects(req, res)
							.then(function(response){
								if (response) {
									var options = {
										ignoreComment: true,
										alwaysChildren: true,
										compact: true
									};
									var updatedIssueXML = xmlJS.json2xml(response, options);
									if (updatedIssueXML) {
										updatedIssueXML = updatedIssueXML.replace(/[\r\n]+[ \t]*/g, '');
										updatedIssueXML = updatedIssueXML.replace(/<#text>.*?<\/#text>/g, '');
										updatedIssueXML = '<projects>' + updatedIssueXML + '</projects>';
										let pl = cheerio.load(updatedIssueXML, {
											recognizeSelfClosing: true
										});
										var projectNode = pl("projects").filter(function (i, n) {
											if (pl(this).find('> name').text() == req.body.project) {
												return pl(this);
											}
										});
										if (projectNode.length == 0) {
											expLogger('addstage: project Not found '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
											reject({
												status: {
													code: '400',
													message: 'Project not found'
												},
												step: "add stage"
											});
											return false;
										}
									} else {
										expLogger('addstage: project file Not found '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
										reject({
											status: {
												code: '400',
												message: 'Project list not found'
											},
											step: "add stage"
										});
										return false;
									}
									//get workflow template
									var workflowFile = './db/kriyadocs/customers/' + req.body.customer + '/config' + projectNode.find('workflowTemplate').text();
									if(req.body.workflowType && req.body.workflowType == 'milestone'){
										var workflowFile = './db/kriyadocs/customers/' + req.body.customer + '/config/default/milestoneWorkflowTemplate.xml';
									}
									workflowFile = path.resolve(workflowFile);
									if (fs.existsSync(workflowFile)) {
										var workflowFileData = fs.readFileSync(workflowFile, 'utf8');
										var wfDom = libxmljs.parseXml(workflowFileData);
									} else {
										expLogger('addstage: workflowTemplate Not found '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
										reject({
											status: {
												code: '400',
												message: 'Workflow not found'
											},
											step: "add stage"
										});
										return false;
									}

									//get stage template from project list
									if (projectNode.find('stageTemplate').length > 0) {
										var stageTemplate = './db/kriyadocs/customers/' + req.body.customer + projectNode.find('stageTemplate').text();
									} else {
										var stageTemplate = './db/kriyadocs/customers/' + req.body.customer + '/config/default/stageTemplate.xml';
									}
									if(req.body.workflowType && req.body.workflowType == 'milestone'){
										stageTemplate = './db/kriyadocs/customers/' + req.body.customer + '/config/default/milestoneTemplate.xml';
									}
									stageTemplate = path.resolve(stageTemplate);
									if (fs.existsSync(stageTemplate)) {
										var stageTemplate = fs.readFileSync(stageTemplate, 'utf8');
										stageTemplate = stageTemplate.replace(/[\n\r]+[\t]*/g, '')
										stageTemplate = stageTemplate.replace(/\s\s\s\s/g, '')
										var stDom = libxmljs.parseXml(stageTemplate);
									} else {
										expLogger('addstage: stageTemplate Not found '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
										reject({
											status: {
												code: '400',
												message: 'Stage template not found'
											},
											step: "add stage"
										});
										return false;
									}

									if (req.body.resupply && req.body.resupply == 'true') {
										var currStage = doc.find("//workflow/stage[last()]");
									} else {
										var currStage = doc.find('//workflow/stage[./*[local-name()="status"] = "in-progress"]');
									}

									//GIT ID #10556 - To get last stage node if article is in banked stage
									// and archive stage - Jai
									if(currStage.length == 0){
										var lastStage = doc.find("//workflow/stage[last()]");
										if(lastStage.length > 0){
											var lastStageName = lastStage[0].find('./name');
											if(lastStageName.length > 0 && /Banked|Archive/i.test(lastStageName[0].text())){
												currStage = lastStage;
											}											
										}
									}

									var pageCount = 0;
									var pageCountTag = doc.find('//article-meta/counts/page-count/@count')
									if(pageCountTag.length > 0){
										pageCount = doc.find('//article-meta/counts/page-count/@count')[0].value()
									}
									//get current stage and then workflow node
									if (currStage.length > 0) {
										currStage = currStage[0];
										var workflowNode = doc.find("//workflow");
										if (workflowNode.length > 0) {
											workflowNode = workflowNode[0];
										}
									} else {
										expLogger('addstage: stage Not found '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
										reject({
											status: {
												code: '400',
												message: 'Stage not found in workflow'
											},
											step: "add stage"
										});
										return false;
									}
									var currStageName = currStage.find('./name');
									if (currStageName.length > 0) {
										currStageName = currStageName[0].text();
									}
									if (req.body.stageName == currStageName) {
										resolve({
											status: {
												code: '200',
												message: 'Current stage name and requested name are same : ' + currStageName
											},
											step: "add stage"
										});
										return false;
									}
									var count = require('./updatewordcount.js')
									count['processapi'](req, res, "false")
										.then(function (countInfo) {
											var customMetaUpdated = false;
											var addCondtionNotes = false;
											var conditionNotesData = '';
											var notesMessageArray = [];
											var conditionNodes = wfDom.find('.//conditions/condition[contains(@stage-name, "|' + currStageName + '|")][contains(@to-stagename, "|' + req.body.stageName + '|")]');
											for (var c = 0, cl = conditionNodes.length; c < cl; c++) {
												var condition = conditionNodes[c];
												//var conditionXpath = condition.attr('xpath').value();
												var insertInto = condition.attr('insertInto').value();
												var insertIntoNode = doc.find(insertInto);
												var insertNode = conditionCheck(condition.find('./if'), doc);
												if (insertIntoNode.length > 0 && insertNode.length > 0) {
													for (var i = 0, inl = insertNode.length; i < inl; i++) {
														var customMeta = libxmljs.parseXml(insertNode[i].toString()).root();
														insertIntoNode[0].addChild(customMeta);
														customMetaUpdated = true;
														var userName = customMeta.find('.//user-name');
														if (userName.length > 0) {
															userName[0].text('Workflow');
														}
														var dateTime = customMeta.find('.//date-time');
														if (dateTime.length > 0) {
															dateTime[0].text(moment().utc().format('YYYY-MM-DD HH:mm:ss'));
														}
													}
													if (condition.attr('notes')) {
														notesMessageArray.push(condition.attr('notes').value());
													}
													if (condition.attr('hold-comment')) {
														if(!req.body.holdComment) req.body.holdComment = '';
														req.body.holdComment = req.body.holdComment + ' ' +condition.attr('hold-comment').value();
													}
												}
											}

											// if release hold, then the stage to be added is previous stage
											var holdFlag = doc.find('//article//article-meta//custom-meta-group/custom-meta[@data-title="hold"][@data-type="pending"][last()]')
											if ((req.body.releaseHold && /^(Hold|Support)$/.test(currStageName)) || (holdFlag.length > 0 && holdFlag[0].attr('specific-use') && holdFlag[0].attr('specific-use').value() == 'hold-release')) {
												var prevStageArr = currStage.find('./preceding-sibling::stage');
												if (prevStageArr.length > 0) {
													var i = 1;
													var prevStage = prevStageArr[prevStageArr.length - i]
													if (prevStage.find('./name').length > 0 && currStageName == 'Hold') {
														req.body.stageName = prevStage.find('./name')[0].text();
													}
													else {
														while (prevStageArr.length - i > 0) {
															prevStage = prevStageArr[prevStageArr.length - i]
															if (prevStage.find('./name').length > 0 && /^(Hold|Support)$/.test(prevStage.find('./name')[0].text())) {
																i++;
															}
															else if (prevStage.find('./name').length > 0) {
																req.body.stageName = prevStage.find('./name')[0].text();
																break
															}

														}
													}


                                            }
										}

											//if custom-meta has attribute to hold on a specific stage, add that specific stage and then add HOLD
											if (holdFlag.length > 0 && holdFlag[0].attr('specific-use') && holdFlag[0].attr('specific-use').value() == 'hold' && holdFlag[0].find('.//meta-name').length > 0) {
												var metaName = holdFlag[0].find('.//meta-name')[0].text();
												var newStage = stDom.find('//stages/stage[./*[local-name()="name"] = "' + metaName + '"]');
												if (newStage.length > 0) {
													newStage = newStage[0].toString();
												} else {
													expLogger('addstage:Could not get ' + metaName + ' from stage template '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
													reject({
														status: {
															code: '400',
															message: 'Could not get ' + metaName + ' from stage template'
														},
														step: "add stage"
													});
													return false;
												}

												//var newStage = new dom().parseFromString(newStage, 'text/xml');
												var newStage = libxmljs.parseXml(newStage).root();
												//var newStage = doc.importNode(newStage.documentElement, true);
												var iterationCount = workflowNode.find('//stage[./*[local-name()="name"] = "' + metaName + '"]').length;
												var stageStatus = newStage.find('./status');
												if (stageStatus.length > 0) {
													if (!/completed/.test(stageStatus[0].text())) {
														stageStatus[0].text('completed');
														var endDate = newStage.find('.//end-date');
														if (endDate.length > 0) {
															endDate[0].text(moment().utc().format('YYYY-MM-DD HH:mm:ss'));
															var startDate = newStage.find('.//start-date');
															// add duration here
															if (startDate.length > 0) {
																startDate[0].text(moment().utc().format('YYYY-MM-DD HH:mm:ss'));
																var diff = Math.round((new Date(endDate[0].text()) - new Date(startDate[0].text())) / (1000 * 60 * 60 * 24))
																var iterationText = ++iterationCount;
																var workflowchildTags = '<duration>'+diff+'</duration><iteration>'+iterationText+'</iteration>';
																if (countInfo && countInfo['word-count'] && countInfo['word-count-without-ref']) {
																	workflowchildTags += '<start-word-count>'+countInfo['word-count']+'</start-word-count><start-word-count-without-ref>'+countInfo['word-count-without-ref']+'</start-word-count-without-ref>'
																	workflowchildTags += '<end-word-count>'+countInfo['word-count']+'</end-word-count><end-word-count-without-ref>'+countInfo['word-count-without-ref']+'</end-word-count-without-ref>'
																}
																var workflowchild = libxmljs.parseXml('<root>'+workflowchildTags+'</root>').root();
																var wfmchildelements =  workflowchild.find('//root/*')
																for (var s = 0, sl = wfmchildelements.length; s < sl; s++) {
																	endDate[0].addNextSibling(wfmchildelements[s])
																}
															}
														}
													}
												}
												workflowNode.addChild(newStage);
												holdFlag[0].attr('data-type', 'processed');
												customMetaUpdated = true;
												req.body.stageName = 'Hold';
											}
											// updating time while signoff
											var currentDateTime = moment().utc().format('YYYY-MM-DD HH:mm:ss');
											currentDateTime = new Date(currentDateTime);
											var currentDate = moment().utc().format('YYYY-MM-DD');
											var currentTime = moment().utc().format('HH:mm:ss');
											var jobLogEndDate = currStage.find('./job-logs/log[last()]/end-date');
											if (jobLogEndDate.length > 0) {
												jobLogEndDate[0].text(currentDate);
											}
											var jobLogEndTime = currStage.find('./job-logs/log[last()]/end-time');
											if (jobLogEndTime.length > 0) {
												jobLogEndTime[0].text(currentTime);
											}
											
											// mark log status as signed-off
											var jobLogStatus = currStage.find('./job-logs/log[last()]/status');
											if (jobLogStatus.length > 0) {
												jobLogStatus[0].text('signed-off');
											}

											var prevStageKeys = ""
											// complete the current stage, add end-date and duration
											var stageStatus = currStage.find('./status');
											if (stageStatus.length > 0) {

												var articleKey = currStage.find('./article-key')
												if(articleKey.length >0){
													prevStageKeys = articleKey[0].toString();
												}
												if (!/completed/.test(stageStatus[0].text())) {
													stageStatus[0].text('completed');
													var endDate = currStage.find('./end-date');
													if (endDate.length > 0) {
														endDate[0].text(moment().utc().format('YYYY-MM-DD HH:mm:ss'));
														var startDate = currStage.find('./start-date');
														// add duration here into minutes by Anuraja
														if (startDate.length > 0) {
															var totalTime = 0;
															var logs = currStage.find('./job-logs/log') //$(currStage).find('job-logs log');
															for (var l = 0, ll = logs.length; l < ll; l++) {
																var log = logs[l];
																if (log.find('./start-time').length > 0 && log.find('./end-time').length > 0) {
																	var startDateTime = log.find('./start-date')[0].text() + " " + log.find('./start-time')[0].text();
																	var endDateTime = log.find('./end-date')[0].text() + " " + log.find('./end-time')[0].text();
																	if (!(startDateTime == undefined || startDateTime == '' || endDateTime == undefined || endDateTime == '')) {
																		var sd = moment(startDateTime, 'YYYY-MM-DD HH:mm:ss');
																		var ed = moment(endDateTime, 'YYYY-MM-DD HH:mm:ss');
																		//var timeTaken = parseInt((ed - sd) / 1000) / 60;
																		var durationMin = moment.duration(ed.diff(sd));
																		totalTime += parseInt(durationMin.asMinutes());
																	}
																}
															}
															var wfmchildTag = '';
															if (countInfo && countInfo['word-count'] && countInfo['word-count-without-ref']) {
																wfmchildTag = '<end-word-count>'+countInfo['word-count']+'</end-word-count><end-word-count-without-ref>'+countInfo['word-count-without-ref']+'</end-word-count-without-ref>'
															}
															var diff = Math.round((new Date(endDate[0].textContent) - new Date(startDate[0].textContent)) / (1000 * 60 * 60 * 24))
															wfmchildTag += '<duration>'+totalTime+'</duration>';
															let wfmchildTags = libxmljs.parseXml('<root>'+wfmchildTag+'</root>').root();
															var wfmchildelements =  wfmchildTags.find('//root/*')
															for (var s = 0, sl = wfmchildelements.length; s < sl; s++) {
																endDate[0].addNextSibling(wfmchildelements[s])
															
															}
														}
													}
												}
											}
											if(req.body.addComment){
												var commentsTag = currStage.find('./comments');
												if(commentsTag.length>0){
													var comments = commentsTag[0].text();
													if(commentsTag[0].text() != '' && !commentsTag[0].text().match(/(\;)$/)) comments += ';';
													commentsTag[0].text(comments + req.body.addComment);
												}
											}

											var newStage = stDom.find('//stages/stage[./*[local-name()="name"] = "' + req.body.stageName + '"]');
											if (newStage.length > 0) {
												newStage = newStage[0].toString();
											} else {
												expLogger('addstage:Could not get ' + req.body.stageName + ' from stage template '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
												reject({
													status: {
														code: '400',
														message: 'Could not get ' + req.body.stageName + ' from stage template'
													},
													step: "add stage"
												});
												return false;
											}
											// add new stage into workflow
											var newStage = libxmljs.parseXml(newStage).root();
											var iterationCount = workflowNode.find('//stage[./*[local-name()="name"] = "' + req.body.stageName + '"]').length;
											var startDate = newStage.find('./name');
											
											//Update the assigned by date as current date and time - Jagan
											var assignedOn = newStage.find('.//assigned/on');
											if(assignedOn.length > 0){
												var assignedOnTime = moment().utc().format('YYYY-MM-DD HH:mm:ss');
												assignedOn[0].text(assignedOnTime);
											}

											//Assign the new stage to user which is configured in workflow trigger - Jagan
											if(assignName && assignEmail && newStage.find('.//assigned/to').length > 0){
												newStage.find('.//assigned/to')[0].text(assignName + ', ' + assignEmail);
												notesMessageArray.push("Article assigned to " + newStage.find('.//assigned/to')[0].text());
											}

											
											// add duration here into minutes by Anuraja
											if (startDate.length > 0) {
												var iterationTagCount = ++iterationCount;
												var wfmchildTag = '<iteration>' + iterationTagCount + '</iteration>';
												if (countInfo && countInfo['word-count'] && countInfo['word-count-without-ref']) {
													wfmchildTag += '<start-word-count>'+countInfo['word-count']+'</start-word-count><start-word-count-without-ref>'+countInfo['word-count-without-ref']+'</start-word-count-without-ref>';
												}
												let wfmchildTags = libxmljs.parseXml('<root>'+wfmchildTag+'</root>').root();
												var wfmchildelements =  wfmchildTags.find('//root/*')
												for (var s = 0, sl = wfmchildelements.length; s < sl; s++) {
													startDate[0].addPrevSibling(wfmchildelements[s])
												}
											}
											if (req.body.customerStage) {
												var customerStage = newStage.find('./customer-stage-name')
												if (customerStage.length > 0) {
													customerStage[0].text(req.body.customerStage);
												}
											}
											if (req.body.heldStage) {
												var heldStage = newStage.find('./held-stage')
												if (heldStage.length > 0) {
													heldStage[0].text(req.body.heldStage);
												}
											}
											if (req.body.holdState) {
												var holdState = newStage.find('./hold-state')
												if (holdState.length > 0) {
													holdState[0].text(req.body.holdState);
												}
											}
											if (req.body.holdComment) {
												var holdComment = newStage.find('./comments')
												if (holdComment.length > 0) {
													holdComment[0].text(req.body.holdComment);
												}
											}

                      //To clone article-keys from prev-stage
											var updatedStage = newStage.find('//stage')
											if(prevStageKeys!="" && newStage.find('./article-key-from-last-stage').length > 0 && updatedStage.length >0){
												newStage.find('./article-key-from-last-stage')[0].remove()
												var articleKey = libxmljs.parseXml(prevStageKeys).root();
												updatedStage[0].addChild(articleKey);
											}

											workflowNode.addChild(newStage);
											var currentStageNode = workflowNode.find('./current-stage')
											if (currentStageNode. length > 0) {
												currentStageNode[0].text(req.body.stageName)
											}
											
											//update Sla date based on sla condition added by vijayakumar
											var slaCondtions = workflowNode.find('.//sla[@sla-condition]');
											for (var s = 0, sl = slaCondtions.length; s < sl; s++) {
												condition = slaCondtions[s].attr('sla-condition').value()
												if(condition && doc.find(condition).length == 0){
													slaCondtions[s].remove()
												}
											}

											var timeVal = {};
											var slaNode = workflowNode.find('.//sla');
											for (var s = 0, sl = slaNode.length; s < sl; s++) {
												var sla = slaNode[s];
												var slaHour = sla.attr('time').value();
												// Checking consecutive days attribute in template or not.
												// If consecutive-days attribute not present, update business days.
												// if consecutive-days attribute present, update consecutive days(include saturday and sunday). 
												if(sla.attr('time') && (!sla.attr('consecutive-days') || (sla.attr('consecutive-days') && sla.attr('consecutive-days').value() != "true"))){
													if (slaHour && slaHour != undefined) {
														timeVal['currentTime'] = moment().utc().format('YYYY-MM-DD HH:mm:ss');
														timeVal['slaStartTime'] = moment().utc().format('YYYY-MM-DD HH:mm:ss');
														timeVal['currentDate'] = moment().utc().format('YYYY-MM-DD HH:mm:ss');
														if(!moment(timeVal['slaStartTime']).isBusinessDay()){
															//changed HH:mm:ss to 00:00:00 by Anuraja to get next business day 00:00:00 isa start time
															timeVal['slaStartTime'] = moment(timeVal['slaStartTime']).nextBusinessDay().format('YYYY-MM-DD 00:00:00')
														}
														var excludeDays = false;
														//Example exclude-day="0,6"; 0 is a Sunday and 6 is a Saturday. by default saturday and sunday will exclude. 
														//If value is none then all days will include
														if(sla.attr('exclude-day')){
															excludeDays = sla.attr('exclude-day').value();
														}
														timeVal['endTime'] = addBusinessDay(slaHour, 'h', timeVal['slaStartTime'], excludeDays);

														if (!(timeVal['endTime'] == undefined || timeVal['endTime'] == '')) {
															var businessDay = moment(timeVal['endTime']).isBusinessDay();
															if (!businessDay && excludeDays == false) {
																var nextBusinessDay = moment(timeVal['endTime']).nextBusinessDay();
																timeVal['endTime'] = nextBusinessDay;
															}
															timeVal['endTime'] = moment(timeVal['endTime']).format('YYYY-MM-DD HH:mm:ss');
															timeVal['endDate'] = moment(timeVal['endTime']).format('YYYY-MM-DD HH:mm:ss');
														}														
													}
												} else if(sla.attr('time') && sla.attr('consecutive-days') && sla.attr('consecutive-days').value() == "true"){
													if (slaHour && slaHour != undefined) {
														timeVal['currentTime'] = moment().utc().format('YYYY-MM-DD HH:mm:ss');
														timeVal['slaStartTime'] = moment().utc().format('YYYY-MM-DD HH:mm:ss');
														timeVal['currentDate'] = moment().utc().format('YYYY-MM-DD HH:mm:ss');
														var day=moment(timeVal['slaStartTime'])														
														timeVal['endTime'] = day.add(slaHour, 'h');															
														timeVal['endTime'] = moment(timeVal['endTime']).format('YYYY-MM-DD HH:mm:ss');
														timeVal['endDate'] = moment(timeVal['endTime']).format('YYYY-MM-DD HH:mm:ss');
													}
												} else{
													reject({
														status: {
															code: '400',
															message: 'Could not update sla date'
														},
														step: "add stage"
													});
													return false;
												}
												var slaChilds = sla.find('./*');
												for (var sc = 0, scl = slaChilds.length; sc < scl; sc++) {
													slaChilds[sc].text(strformat(slaChilds[sc].text(), timeVal));
													//sla.parent.insertBefore(slaChilds[sc], sla)
													sla.addPrevSibling(slaChilds[sc])
												}
												sla.remove();
											}
											//Added for maintain previous sla date for support
											//Added addjob, file download, convert files by Anuraja 2018-09-26
											var stages = /(Support|addJob|File download|Convert Files|Hold)/;
											var stageToaddComment = /(Support|Hold|addJob)/;
											var commentText = "Moved to ";
											var lastStage = workflowNode.find('./stage[last()]')[0];
											var stageName = lastStage.find('./name')[0].text();
											var prevStage = lastStage.find('./preceding-sibling::stage');
											var prevStageName = "";
											var prevAssignedUser = "";
											var prePrevAssigendUser = "";
											var prePrevStage = "";
											if (prevStage.length > 0) {
												if (prevStage.length > 1) {
													prePrevStage = prevStage[prevStage.length - 2];
													prePrevAssigendUser = prePrevStage.find('./assigned/to')[0].text();
												}
												prevStage = prevStage[prevStage.length - 1];
												prevStageName = prevStage.find('./name')[0].text();
												var prevAssignedUser = prevStage.find('./assigned/to', prevStage)[0].text();
												var prevStageComment = prevStage.find('./comments');
												if (prevStageComment.length > 0) {
													var prevStageCommentText = prevStageComment[0].text();
													if (prevStageCommentText != '' && prevStageCommentText != undefined) {
														prevStageCommentText += ', ';
													}
												}
												//Updated by Anuraja for git-1714 on 01-11-2018 - To add comment in previous stage while pap resupply: Will add comment if req.body.addStageComment == true
												if (stageName.match(stageToaddComment) || req.body.addStageComment == 'true') {
													if (prevStageComment.length > 0) {
														prevStageComment[0].text(prevStageCommentText + commentText + stageName);
													}
												}
											}
											if (stageName.match(stages)) {
												if (prevStage.find('.//sla-start-date').length > 0 && prevStage.find('.//sla-end-date').length > 0) {
													if (lastStage.find('.//sla-start-date').length > 0 && lastStage.find('.//sla-end-date').length > 0) {
														lastStage.find('.//sla-start-date')[0].text(prevStage.find('.//sla-start-date')[0].text());
														lastStage.find('.//sla-end-date')[0].text(prevStage.find('.//sla-end-date')[0].text())
													}
													if (prevStage.find('.//customer-stage-name').length > 0) {
														lastStage.find('.//customer-stage-name')[0].text(prevStage.find('.//customer-stage-name')[0].text())
													}
												}
											} else {
												if (prevStageName.match(stages)) {
													if (prevStage.find('.//sla-start-date').length > 0 && prevStage.find('.//sla-end-date').length > 0) {
														if (lastStage.find('.//sla-start-date').length > 0 && lastStage.find('.//sla-end-date').length > 0) {
															lastStage.find('.//sla-start-date')[0].text(prevStage.find('.//sla-start-date')[0].text());
															lastStage.find('.//sla-end-date')[0].text(prevStage.find('.//sla-end-date')[0].text())
														}
														if (prevStage.find('.//customer-stage-name').length > 0) {
															lastStage.find('.//customer-stage-name')[0].text(prevStage.find('.//customer-stage-name')[0].text())
														}
													}
												}
											}
											if(stageName.match(/support/i) && process.env.NODE_ENV == 'production'){
												var taskmanMessage = "Article " + req.body.doi+ " moved to support"
												updateTaskManStatus(req.body, taskmanMessage)
											}

											//added to retain assignee name on hold from previous stage - Jai 15-Feb-2021
											if(stageName.match(/Hold/i)){
												if (lastStage.find('.//assigned/to').length > 0){
													lastStage.find('.//assigned/to')[0].text(prevAssignedUser);
												}
											}
											//Added by Anuraja to reassign the user who worked on previously
											let assignPrePreviousUserStages = /^(Support|Hold)$/
											if(prevStageName.match(assignPrePreviousUserStages)){
												if (lastStage.find('.//assigned/to').length > 0){
													lastStage.find('.//assigned/to')[0].text(prePrevAssigendUser);
												}
												//add article-key from previous stage if the previous stage to support was author related stage
												if(/Author/i.test(stageName)){
													var prevStage = lastStage.find('./preceding-sibling::stage');
													if (prevStage.length > 0) {
														if (prevStage.length > 1) {
															prePrevStage = prevStage[prevStage.length - 2];
															let prePrevStageName = prePrevStage.find('./name')[0].text();
															// check previous stage to support and current stage name are equal
															if (prePrevStageName == stageName){
																var preArticleKey = prePrevStage.find('./article-key');
																if (preArticleKey.length > 0){
																	var currArticleKey = libxmljs.parseXml(preArticleKey[0].toString()).root();
																	lastStage.addChild(currArticleKey);
																}
															}
														}
													}
												}
											}
											var assignUserFromStageTag = workflowNode.find('.//assign-user-from-stage');
											if(assignUserFromStageTag.length > 0){
												var userToAssign = workflowNode.find('.//stage[./*[local-name()="name"] = "' + assignUserFromStageTag[0].text() + '"]/assigned/to')
												if(userToAssign.length > 0 ){
													let userNameAssigned = userToAssign[0].text();
													if (lastStage.find('.//assigned/to').length > 0){
														lastStage.find('.//assigned/to')[0].text(userNameAssigned);
													}
												}
												let getCurrentPreviousTag = workflowNode.find('.//stage[./*[local-name()="name"] = "' + stageName + '"]/assigned/to')
												if(getCurrentPreviousTag.length > 1){
													let getCurrentPreviousTagValue = getCurrentPreviousTag[getCurrentPreviousTag.length - 2].text();
													if (lastStage.find('.//assigned/to').length > 0){
														lastStage.find('.//assigned/to')[0].text(getCurrentPreviousTagValue);
													}
												}
												notesMessageArray.push("Article assigned to " + lastStage.find('.//assigned/to')[0].text());
												assignUserFromStageTag[0].remove();
											}
											// to add notes when a new stage has been added - JAI - 07-09-2018
											var currTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
											if (prevStageName != '') {
												notesMessageArray.push("Article signed-off from " + prevStageName + " to " + stageName);
											} else {
												notesMessageArray.push("Article signed-off to " + stageName);
											}
											var commentJSON = {
												"content": notesMessageArray,
												"created": currTime
											}
											req.body.notesData = {
												"content": commentJSON,
												"doi": req.body.doi,
												"customer": req.body.customer,
												"project": req.body.project,
												"noteType": "workflow"
											};
											// to save support notes to the production notes at the time of add stage
											if (req.body.supportComment && req.body.supportComment.content){
												req.body.notesData.supportComment = req.body.supportComment;
											}
											if (req.body.faqComment && req.body.faqComment.content){												
												fs.writeFile('./FAQ/'+(req.body.faqComment.faqID ? req.body.faqComment.faqID : uuidv4())+'.xml', req.body.faqComment.content, function(err){
													if(err) {
														console.log('ERROR : Unable to write faq File'+err);
														expLogger('addstage: Unable to write faq File' + req.body.stagename + ' '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
													}else{
														console.log('SUCCESS : FAQ file written ');
														expLogger('addstage: FAQ file written' + req.body.stagename + ' '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'success', 'addDate': 'true'});
													}
												})
											}
											expLogger('Stage ' + req.body.stagename + ' successfully added', {
												'logFileName': req.body.doi,
												'req': req
											});
											var saveObj = [];
											var cutomMetaGroup = doc.find('//custom-meta-group');
											if (cutomMetaGroup.length > 0 && customMetaUpdated) {
												cutomMetaGroup = cutomMetaGroup[0];
												var metaString = cutomMetaGroup.toLocaleString();
												if(metaString && !metaString.match(/<custom-meta-group xmlns:xlink="http:\/\/www.w3.org\/1999\/xlink"/)){
													cutomMetaGroup.attr('xmlns:xlink', "http://www.w3.org/1999/xlink");
												}											

												cutomMetaGroup = '<?xml version="1.0" encoding="utf-8"?>' + cutomMetaGroup.toLocaleString().replace(/[\r\n\t]/g, '');
												cutomMetaGroup = cutomMetaGroup.replace(/\s+/g, ' ');
												cutomMetaGroup = {'process' : 'update', 'update': '//custom-meta-group', 'content': cutomMetaGroup}
												saveObj.push(cutomMetaGroup)
											} else {
												cutomMetaGroup = false;
											}
											var workflowXML = workflowNode.toLocaleString().replace(/[\r\n\t]/g, '');
											workflowXML = workflowXML.replace(/\s+/g, ' ')
											var wfObj = { 'process': 'update', 'update': '//workflow', 'content': workflowXML }
											saveObj.push(wfObj)
											// Update by Prasana on 11-07-2019 to update/create count details in article xml.
											if (countInfo && Object.keys(countInfo).length) {
												if (countInfo['word-count-without-ref'] != undefined) {
													saveObj.push({ 'process': 'update', 'update': '//article/workflow/word-count-without-ref', 'content': '<word-count-without-ref>' + countInfo['word-count-without-ref'] + '</word-count-without-ref>', 'append': '//article/workflow' });
												}
												countInfo['page-count'] = pageCount;
												var eachCountTag = '';
												var orderOfCounts = ['fig-count', 'table-count', 'equation-count', 'ref-count', 'page-count', 'word-count'];
												for (var node of orderOfCounts) {
													var countTag = '';
													if (countInfo[node]) {
														countTag = '<'+node+' count="'+countInfo[node]+'"></'+node+'>';
														saveObj.push({ 'process': 'update', 'update': '//article/front/article-meta/counts/'+node, 'content': countTag, 'append': '//article/front/article-meta/counts', 'parent-node': 'counts', 'parent-append': '//article/front/article-meta' });
													}
												}
											}

											// Converting the custom meta name and custom meta value to custom obj
											// customObj structure: {customMetaName: "custom meta name", customMetaValue: "custom meta value", customMetaAttrib": {}}
											if(req.body.customMetaName && req.body.customMetaValue){	
												if(!req.body.customObj){
													req.body.customObj = [];
												}
												var curCustomMeta = {}
												curCustomMeta["customMetaName"] = req.body.customMetaName,
												curCustomMeta["customMetaValue"] = req.body.customMetaValue

												if(req.body.customMetaAttrib && !(req.body.customMetaAttrib instanceof Array) && typeof (req.body.customMetaAttrib) =='object' ){
													curCustomMeta["customMetaAttrib"] = req.body.customMetaAttrib;
												}	

												req.body.customObj.push(curCustomMeta)
											}

											if(req.body.customObj != undefined && req.body.customObj.length > 0){
												var customGroup = req.body.customObj;
												for(cmIdx = 0; cmIdx < customGroup.length; cmIdx++){
													let { customMetaName, customMetaValue, customMetaAttrib } = customGroup[cmIdx];
													
													let customMetaAttribValue = "";//Updated by Anuraja to add attribute into custom meta
												
													if(customMetaAttrib && !(customMetaAttrib instanceof Array) && typeof (customMetaAttrib) =='object' ){
														customAttribs = customMetaAttrib;
														for (var attribs of Object.keys(customAttribs)){
															customMetaAttribValue += ' '+attribs +'="'+customAttribs[attribs]+'"';
														}
													}

													cutomMetaGroup = {
														'process' : 'update', 
														'update': '//custom-meta-group/custom-meta[meta-name[.="'+customMetaName+'"]]',
														'content': '<custom-meta'+customMetaAttribValue+'><meta-name>'+customMetaName+'</meta-name><meta-value>'+customMetaValue+'</meta-value></custom-meta>',
														'append': '//article//custom-meta-group'
													}
													saveObj.push(cutomMetaGroup)
												}
											}										

											//update custom meta group
											updateXMLData(req, res, saveObj)
												.then(function (response) {
													req.body.updatingXML = false;
													resolve(response);
													var api = require('./save_note.js');
													return api['processapi'](req, res, req.body.notesData)
												})
												.then(function (responseFromNotes) {
													updateElastic(req, res)
												})
												.catch(function (err) {
													req.body.updatingXML = false;
													expLogger('addstage: failed to update XML' + req.body.stagename + ' '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
													expLogger('Add stage failed: ' + req.body.stagename + ':\n' + err, {
														'logFileName': req.body.doi,
														'addDate': 'true',
														'req': req
													});
													//reject(err);
													reject({
														status: {
															code: '400',
															message: 'Could not update data to database'
														},
														step: "add stage"
													});
													return false;
												});
										})
										.catch(function (err) {
											if (typeof (err) == 'object') {
												err = JSON.stringify(err);
											}
											expLogger('addstage: failed to update wordcount '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
											reject({
												status: {
													code: '400',
													message: 'Could not update word count : ' + err
												},
												step: "add stage"
											});
										});

								} else {
									expLogger('addstage: Project List not found '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
									reject({
										status: {
											code: '400',
											message: 'Project List not found'
										},
										step: "add stage"
									});
									return false;
								}
							})
							.catch(function(err){
								expLogger('addstage: Project List not found '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
								reject({
									status: {
										code: '400',
										message: 'Project List not found in database'
									},
									step: "add stage"
								});
							})
					}
				})
				.catch(function (err) {
					expLogger('addstage: Could not fetch XML '+req.body.doi, {'logName': 'api', 'logType': 'process', 'logStatus': 'failure', 'addDate': 'true'});
					reject({
						status: {
							code: '400',
							message: 'Could not fetch XML'
						},
						step: "add stage"
					});
				});
		});
	}
}

function getXMLData(url) {
	return new Promise(function(resolve, reject) {
		unirest.get(url)
			.type('xml')
			.send()
			.end(response=> {
				resolve(response);
			});
	})
}

function conditionCheck(condition, doc) {
	if (condition.length > 0 && condition[0].attr('xpath') && doc.find(condition[0].attr('xpath').value()).length > 0) {
		if (condition[0].find('./if').length > 0) {
			return conditionCheck(condition[0].find('./if'), doc);
		} else {
			return condition[0].find('./*');
		}
	} else {
		return false;
	}
}

function getProjects(req, res){
	return new Promise(function(resolve, reject) {
		var projectsApi = require('../../api/get/projects.js');
		projectsApi['processapi'](req, res)
			.then(function(response){
				if (response.length == 1 && response[0]._source){
					resolve(response[0]._source);
				}else{
					reject(response)
				}
			})
			.catch(function(err){
				reject(err)
			})	
	});
}

function updateXMLData(req, res, saveObj){
	return new Promise(function(resolve, reject) {
		var updateData = require('../../api/post/updatedata.js');
		updateData['update'](req, res, saveObj)
			.then(function(response){
				resolve(response.body);
			})
			.catch(function(err){
				reject(err)
			})	
	});
}
//Added by Anuraja to calculate businessdays
function addBusinessDay(number, period, time, excludeDay) {
	{
		var day = moment(time);
		var splittedExcludeDay = (excludeDay == false && typeof(excludeDay) != "string")?[]:excludeDay.split(',');
		if (!day.isValid()) {
			return moment(day);
		}
		if (number < 0) {
			number = Math.round(-1 * number) * -1;
		} else {
			number = Math.round(number);
		}
		var signal = number < 0 ? -1 : 1;
		period = typeof period !== 'undefined' ? period : 'days';
		var remaining = Math.abs(number);
		while (remaining > 0) {
			day.add(signal, period);
			//If excludeDay is false then check isBusinessDay
			//If excludeDay is none then don't skip any days
			//Skip the days which is in splittedExcludeDay
			if (((excludeDay == false && day.isBusinessDay()) || (excludeDay == 'none')) || (splittedExcludeDay.length > 0 && splittedExcludeDay.indexOf(day.day().toString()) < 0)) {
				remaining--;
			}
		}
		return day;
	}
}
/**
 * Added by Anuraja on 2019-Oct-05
 * function to update status to taskman
 * @param {*} data
 */
function updateTaskManStatus(data, message){
	var dataToPost = {
		"customer": data.customer,
		"project": data.project,
		"doi": data.doi,
		"message": message
	};
	var request = unirest("POST", cms.config.taskman.url+'/api/updatestatus');
	request.headers({
		"cache-control": "no-cache",
		"content-type": "application/json"
	});
	request.send(dataToPost);
	request.end(function (response) {

	});
}
function getArticle(req, res) {
	return new Promise(function (resolve, reject) {
			var xpath;
			if(req.body.workflowType && req.body.workflowType == 'milestone'){
				xpath= '//container-xml'
			}
			var getData = require('../../api/get/getdata.js');
			getData['processapi'](req, res,xpath)
				.then(function (response) {
					resolve(response);
				})
				.catch(function (err) {
					resolve(false)
				})
	});
}
